$(document).ready(function () {

    // roomDropDown();
    roomDropDownLoader();

    $('#submit').click(function() {
        roomScans();
        scanBreakDown();
    });

});

function roomDropDownLoader() {
    $('#buildingDropDown').change(function(event) {
        event.preventDefault();

        var buildingId = $('#buildingDropDown').val();

        var url = 'http://shamiual2.com/api/v1/room';

        console.log(url);
        console.log('1');

        jQuery.ajax({
            method: 'GET',
            url: url,
            data: {
                "buildingId": buildingId
            },
            dataType: 'json',
            success: function(data) {

                var roomDropDownList = document.getElementById("roomDropDown");

                $.each(data.data, function(index, value){
                    var option = document.createElement("option");
                    option.text = value.name;
                    option.value = value.id;
                    option.innerText = value.name;

                    roomDropDownList.appendChild(option);
                });

            }
        })
    });
}

function scanBreakDown() {

    var url = 'http://shamiual2.com/api/v1/room/break_down_scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function(data) {

            if (data.data.length > 0) {

                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: 'Break Down of Scans'
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e. %b',
                            year: '%b'
                        },
                        title: {
                            text: 'Date'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Scans'
                        },
                        allowDecimals: false,
                        min: 0
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Scans',
                        data: data.data
                    }]
                });
            } else {
                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'No data in pie chart'
                    },
                    series: [{
                        data: []
                    }]
                });
            }
        }
    });

}

// function mostPopularDate() {
//
// }



