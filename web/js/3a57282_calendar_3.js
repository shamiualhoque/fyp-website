$( function() {

    $( "#fromDatePicker" ).datepicker(
        {
            dateFormat: 'dd/mm/yy',
            onSelect: function() {
                $("#toDatePicker").removeAttr("disabled");
            }
        });

    $("#toDatePicker").datepicker(
        {
            dateFormat: 'dd/mm/yy',
            beforeShow: function () {
                $(this).datepicker('option', 'minDate', $('#fromDatePicker').val());
            }
        }
    );
} );