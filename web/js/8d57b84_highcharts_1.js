
function top5rooms() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/mostPopular/room';

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey
        },
        dataType: 'json',
        success: function (data) {
            Highcharts.chart('topFiveRoomsContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Top 5 Most Popular Rooms'
                },
                xAxis: {
                    categories: data.data.name
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data: data.data.scans
                }]
            });
        }
    });
}

function mostPopularBuilding() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/mostPopular/building';

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey
        },
        dataType: 'json',
        success: function (data) {

            var data = data.data;

            var scan = 0;
            var name = "no data";

            if (data != null) {
                scan = parseInt(data.scans);
                name = data.name;
            }

            Highcharts.chart('mostPopularBuildingContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Most Popular Building'
                },
                xAxis: {
                    categories: [ name ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ scan ]
                }]
            });
        }
    });
}

function roomScans()  {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();
    var roomName = $('#roomDropDown option:selected').text();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function (data) {
            var data = data.data;

            Highcharts.chart('roomScansContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Scans Done'
                },
                xAxis: {
                    categories: [ roomName ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ parseInt(data.scans) ]
                }]
            });
        }
    });
}

function buildingBreakChart()  {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/building/scans';

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey
        },
        dataType: 'json',
        success: function (data) {
            var data = data.data;
            Highcharts.chart('buildingBreakDownContainer', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Scans Done In Buildings'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%e. %b',
                        year: '%b'
                    },
                    title: {
                        text: 'Date'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Scans'
                    },
                    min: 0,
                    allowDecimals: false
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: data
            });
        }
    });
}

function allBuildingsTotalScans()
{
    var pathname = window.location.origin;

    var url = pathname + '/api/v1/building/total/scans';

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey
        },
        dataType: 'json',
        success: function (data) {
            var data = data.data;

            Highcharts.chart('allBuildingsTotalScansContainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Scans Done From All Buildings'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: 'Scans',
                    data:
                        data
                    ,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        }

    });
}


