$(document).ready(function () {

    $('#buildingDropDown').change(function(event) {
        event.preventDefault();

        var buildingId = $('#buildingDropDown').val();

        var url = 'http://shamiual2.com/api/v1/room';

        console.log(url);
        console.log('1');

        jQuery.ajax({
            method: 'GET',
            url: url,
            data: {
                "buildingId": buildingId
            },
            dataType: 'json',
            success: function(data) {

                var roomDropDownList = document.getElementById("roomDropDown");

                $.each(data.data, function(index, value){
                    var option = document.createElement("option");
                    option.text = value.name;
                    option.value = value.id;
                    option.innerText = value.name;

                    roomDropDownList.appendChild(option);
                });

            }
        })
    });

});

