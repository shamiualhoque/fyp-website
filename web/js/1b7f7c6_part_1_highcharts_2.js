
function top5rooms() {
    var url = 'http://shamiual2.com/api/v1/mostPopular/room';

    jQuery.ajax({
        method: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {
            Highcharts.chart('topFiveRoomsContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Top 5 Most Popular Rooms'
                },
                xAxis: {
                    categories: data.data.name
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data: data.data.scans
                }]
            });
        }
    });
}

function mostPopularBuilding() {
    var url = 'http://shamiual2.com/api/v1/mostPopular/building';

    jQuery.ajax({
        method: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {

            var data = data.data;

            Highcharts.chart('mostPopularBuildingContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Most Popular Building'
                },
                xAxis: {
                    categories: [ data.name ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ parseInt(data.scans) ]
                }]
            });
        }
    });
}

function roomScans()  {
    var url = 'http://shamiual2.com/api/v1/room/scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();
    var roomName = $('#roomDropDown option:selected').text();

    console.log(roomId);
    console.log(dateFrom);
    console.log(dateTo);

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function (data) {
            var data = data.data;

            Highcharts.chart('roomScansContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Scans Done'
                },
                xAxis: {
                    categories: [ roomName ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ parseInt(data.scans) ]
                }]
            });
        }
    });
}


