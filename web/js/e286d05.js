
function top5rooms() {
    var url = 'http://shamiual2.com/api/v1/mostPopular/room';

    jQuery.ajax({
        method: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {
            Highcharts.chart('topFiveRoomsContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Top 5 Most Popular Rooms'
                },
                xAxis: {
                    categories: data.data.name
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data: data.data.scans
                }]
            });
        }
    });
}

function mostPopularBuilding() {
    var url = 'http://shamiual2.com/api/v1/mostPopular/building';

    jQuery.ajax({
        method: 'GET',
        url: url,
        dataType: 'json',
        success: function (data) {

            var data = data.data;

            Highcharts.chart('mostPopularBuildingContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Most Popular Building'
                },
                xAxis: {
                    categories: [ data.name ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ parseInt(data.scans) ]
                }]
            });
        }
    });
}

function roomScans()  {
    var url = 'http://shamiual2.com/api/v1/room/scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();
    var roomName = $('#roomDropDown option:selected').text();

    console.log(roomId);
    console.log(dateFrom);
    console.log(dateTo);

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function (data) {
            var data = data.data;

            Highcharts.chart('roomScansContainer', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Scans Done'
                },
                xAxis: {
                    categories: [ roomName ]
                },
                yAxis: {
                    title: {
                        text: 'Number of Scans'
                    },
                    allowDecimals: false
                },
                series: [{
                    name: 'Scans',
                    data:  [ parseInt(data.scans) ]
                }]
            });
        }
    });
}



$(document).ready(function () {

    // roomDropDown();
    roomDropDownLoader();

    $('#submit').click(function() {
        roomScans();
        scanBreakDown();
    });

});

function roomDropDownLoader() {
    $('#buildingDropDown').change(function(event) {
        event.preventDefault();

        var buildingId = $('#buildingDropDown').val();

        var url = 'http://shamiual2.com/api/v1/room';

        console.log(url);
        console.log('1');

        jQuery.ajax({
            method: 'GET',
            url: url,
            data: {
                "buildingId": buildingId
            },
            dataType: 'json',
            success: function(data) {

                var roomDropDownList = document.getElementById("roomDropDown");

                $.each(data.data, function(index, value){
                    var option = document.createElement("option");
                    option.text = value.name;
                    option.value = value.id;
                    option.innerText = value.name;

                    roomDropDownList.appendChild(option);
                });

            }
        })
    });
}

function scanBreakDown() {

    var url = 'http://shamiual2.com/api/v1/room/break_down_scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function(data) {

            if (data.data.length > 0) {

                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: 'Break Down of Scans'
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e. %b',
                            year: '%b'
                        },
                        title: {
                            text: 'Date'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Scans'
                        },
                        allowDecimals: false,
                        min: 0
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Scans',
                        data: data.data
                    }]
                });
            } else {
                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'No data in pie chart'
                    },
                    series: [{
                        data: []
                    }]
                });
            }
        }
    });

}

// function mostPopularDate() {
//
// }



