<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="bookings")
 */
class Bookings
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    protected $room_id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $time_from;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $time_to;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $cancelled_time;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId()
    {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id)
    {
        $this->room_id = $room_id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTimeFrom()
    {
        return $this->time_from;
    }

    /**
     * @param mixed $time_from
     */
    public function setTimeFrom($time_from)
    {
        $this->time_from = $time_from;
    }

    /**
     * @return mixed
     */
    public function getTimeTo()
    {
        return $this->time_to;
    }

    /**
     * @param mixed $time_to
     */
    public function setTimeTo($time_to)
    {
        $this->time_to = $time_to;
    }

    /**
     * @return mixed
     */
    public function getCancelledTime()
    {
        return $this->cancelled_time;
    }

    /**
     * @param mixed $cancelled_time
     */
    public function setCancelledTime($cancelled_time)
    {
        $this->cancelled_time = $cancelled_time;
    }

}