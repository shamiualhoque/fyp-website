<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rooms")
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Building")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    protected $building_id;

    /**
     * @ORM\Column(type="string", length=16)
     */
    protected $nfc_code;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBuildingId()
    {
        return $this->building_id;
    }

    /**
     * @param mixed $building_id
     */
    public function setBuildingId($building_id)
    {
        $this->building_id = $building_id;
    }

    /**
     * @return mixed
     */
    public function getNfcCode()
    {
        return $this->nfc_code;
    }

    /**
     * @param mixed $nfc_code
     */
    public function setNfcCode($nfc_code)
    {
        $this->nfc_code = $nfc_code;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}