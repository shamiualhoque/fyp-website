<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\SecurityContext;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class ProjectExtension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, TokenStorage $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function getGlobals()
    {
        return [
            'primany' => '#0000000',
        ];
    }

    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}