<?php

namespace AppBundle\Service;

use AppBundle\Entity\Room;
use Doctrine\ORM\EntityManager;

class RoomManager
{
    /**
     * @var EntityManager
     */
    private $entityManger;

    /**
     * RoomManager constructor.
     *
     * @param $entityManager EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAllRooms()
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Room::class)->findAll();
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}