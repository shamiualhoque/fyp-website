<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ApiKeyUserProvider implements UserProviderInterface
{
    /**
    * @var EntityManager
    */
    private $entityManager;

    /**
     * @var
     */
    private $container;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public function getUsernameForApiKey($apiKey)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
//        $username = ...;

        $username = $this->entityManager->createQuery(
            'SELECT u.username FROM AppBundle:User u WHERE u.apiKey = :apiKey'
        )->setParameter('apiKey', $apiKey)->getSingleScalarResult();

        return $username;
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_USER')
        );

    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        return $user;
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}