<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Bookings;
use AppBundle\Entity\Building;
use AppBundle\Entity\Room;
use AppBundle\Entity\Scans;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ApiBundle:Default:index.html.twig', array('name' => $name));
    }

    /**
     * Adds a scan
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postScansAction(Request $request)
    {
        $userId = $request->query->get("userId");
        $roomId = $request->query->get("roomId");
        $date = $request->query->get("date");

        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $userId = $em->getRepository(User::class)->find($userId);
        $roomId = $em->getRepository(Room::class)->find($roomId);
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);

        $scanEntry = new Scans();
        $scanEntry->setDateTime($date);
        $scanEntry->setUserId($userId);
        $scanEntry->setRoomId($roomId);

        $em->persist($scanEntry);
        $em->flush();

        return new JsonResponse([]);
    }


    /**
     * checks users username with password, if they are correct.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkUserAction(Request $request)
    {
        $username = $request->query->get('username');
        $password = $request->query->get('password');

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $username]);

        if (!$user) {
            return new JsonResponse([
                'success' => false
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($user) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            $bool = $encoder->isPasswordValid($user->getPassword(), $password, null);

            if ($bool == false) {
                return new JsonResponse([
                    'success' => false,
                ], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse([
                'success' => true,
                'data' => [
                    'id' => $user->getId(),
                    'username' => $username
                ]
            ], Response::HTTP_OK);

        } else {
            return new JsonResponse([
                'success' => false
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Returns all buildings
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getRoomsFromBuildingAction(Request $request)
    {
        $roomManager = $this->container->get('booking_manager');

        $buildingId = $request->query->get('buildingId');

        $rooms = $roomManager->getRoomsFromBuildingId($buildingId);

        return new JsonResponse([
            'success' => true,
            'data' => $rooms
        ]);
    }

    /**
     * returns room from NFC_code
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getRoomFromNFCAction(Request $request)
    {
        $roomManager = $this->container->get('booking_manager');

        $nfcCode = $request->query->get("nfcCode");

        $room = $roomManager->getRoomFromNfcCode($nfcCode);

        return new JsonResponse([
            'success' => true,
            'data' => $room
        ]);
    }

    /**
     * Returns all the avialable times for a room.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailableFromTimeAction(Request $request)
    {
        $bookingManager = $this->container->get('booking_manager');
        $roomManager = $this->container->get('room_manager_web');

        $date = $request->query->get('date');
        $roomId = $request->query->get('roomId');


        if ($date) {
            $date = str_replace('/', '-', $date);
            $date = date('Y/m/d', strtotime($date));
        } else {
            $date = date('Y/m/d');
        }

        $room = $roomManager->getRoomById($roomId);

        $booked = $bookingManager->getAvailableTimesFrom($date, $room);

        return new JsonResponse([
            'success' => true,
            'data' => $booked
        ]);
    }

    /**
     * Gets Available to time, for a room booking
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAvailableToTimeAction(Request $request)
    {
        $bookingManager = $this->container->get('booking_manager');

        $availableTimes = $request->query->get('availableTimes');
        $selectedTime = $request->query->get('selectedTime');

        $availableTimes = json_decode($availableTimes);

        $times = $bookingManager->getAvailableTimesTo($availableTimes, $selectedTime);

        return new JsonResponse([
            'success' => true,
            'data' => $times
        ]);
    }

    /**
     * Returns all Buildings
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getBuildingAction(Request $request)
    {
        $bookingManager = $this->container->get('booking_manager');

        $buildings = $bookingManager->getBuilding();

        return new JsonResponse([
            'success' => true,
            'data' => $buildings
        ]);
    }

    /**
     * Get a bookings for a user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getBookingsAction(Request $request)
    {
        $bookingManager = $this->container->get('booking_manager');

        $userId = $request->query->get('userId');

        /** @var Bookings $bookings */
        $bookings = $bookingManager->getBookingsForUser($userId);

        $results = [];

        foreach ($bookings as $booking) {
            $id = $booking->getId();
            $name = $booking->getRoomId()->getName();
            $date = $booking->getDate()->format('d/m/Y');
            $timeFrom = $booking->getTimeFrom()->format('H:i');
            $timeTo = $booking->getTimeTo()->format('H:i');

            array_push($results, [
                'id' => $id,
                'name' => $name,
                'date' => $date,
                'timeFrom' => $timeFrom,
                'timeTo' => $timeTo
            ]);
        }

        return new JsonResponse([
            'success' => true,
            'data' => $results
        ]);
    }

    /**
     * Get any bookings a user will have today
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTodayBookingsAction(Request $request)
    {
        $userId = $request->query->get('userId');

        $bookingManager = $this->container->get('booking_manager');

        /** @var Bookings $bookings */
        $bookings = $bookingManager->getBookingForUserToday($userId);

        $results = [];

        foreach ($bookings as $booking) {
            $id = $booking->getId();
            $name = $booking->getRoomId()->getName();
            $date = $booking->getDate()->format('d/m/Y');
            $timeFrom = $booking->getTimeFrom()->format('H:i');
            $timeTo = $booking->getTimeTo()->format('H:i');

            array_push($results, [
                'id' => $id,
                'name' => $name,
                'date' => $date,
                'timeFrom' => $timeFrom,
                'timeTo' => $timeTo
            ]);
        }

        return new JsonResponse([
            'success' => true,
            'data' => $results
        ]);
    }

    /**
     * delete a booking
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBookingAction(Request $request)
    {

        $bookingId = $request->query->get('bookingId');

        $bookingManager = $this->container->get('booking_manager');

        $bookingManager->deleteBooking($bookingId);

        return new JsonResponse([
          'success' => true
        ]);
    }

    /**
     * create a bookings for a room
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postMakeBookingAction(Request $request)
    {
        $bookingManager = $this->container->get('booking_manager');

        $userId = $request->query->get('userId');
        $roomId = $request->query->get('roomId');
        $date = $request->query->get('date');
        $timeFrom = $request->query->get('timeFrom');
        $timeTo = $request->query->get('timeTo');

        if ($date) {
            $date = str_replace('/', '-', $date);
            $date = date('Y/m/d', strtotime($date));
        } else {
            $date = date('Y/m/d');
        }

        $booking = $bookingManager->makeBooking($userId, $roomId, $date, $timeFrom, $timeTo);

        return new JsonResponse([
            'success' => $booking,
        ]);
    }

    /**
     * Returns the most popular rooms
     *
     * @return JsonResponse
     */
    public function getMostPopularRoomsAction()
    {
        $roomManager = $this->container->get("room_manager_api");

        $roomScans = $roomManager->topFivePopularRooms();

        $name = [];
        $scans = [];

        foreach ($roomScans as $key => $value) {
            array_push($name, $value['name']);
            array_push($scans, (int) $value['scans']);
        }

        return new JsonResponse([
            'success' => true,
            'data' => [
                'name' => $name,
                'scans' => $scans
            ]
        ]);
    }

    /**
     * Returns the most popular building, thorughout the campus.
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMostPopularBuildingAction()
    {
        $roomManager = $this->container->get("room_manager_api");

        $building = $roomManager->mostPopularBuilding();

        return new JsonResponse([
            'success' => true,
            'data' => $building
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRoomsScansAction(Request $request)
    {
        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $scans = $roomManager->getRoomScans($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $scans
        ]);
    }

    public function getRoomsScansBreakDownAction(Request $request) {

        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $scansBreakDown = $roomManager->getScansBreakDown($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $scansBreakDown
        ]);
    }

    public function getBuildingScansAction(Request $request)
    {
        $roomManager = $this->container->get("room_manager_api");

        $scansBreakDown = $roomManager->getBuildingScans();

        return new JsonResponse([
            'success' => true,
            'data' => $scansBreakDown
        ]);
    }

    public function getMostPopularDateForRoomAction(Request $request)
    {
        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $scansBreakDown = $roomManager->getMostPopularDateForRoom($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $scansBreakDown
        ]);
    }

    public function getMostPopularTimeForRoomAction(Request $request)
    {
        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $scansBreakDown = $roomManager->getMostPopularTimeForRoom($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $scansBreakDown
        ]);
    }

    public function getAllBuildingTotalScansAction()
    {
        $roomManager = $this->container->get("room_manager_api");

        $buildingsTotalScans = $roomManager->getAllBuildingsTotalScans();

        return new JsonResponse([
            'success' => true,
            'data' => $buildingsTotalScans
        ]);
    }

    public function getBookingsMadeForRoomAction(Request $request)
    {
        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $bookings = $roomManager->getBookingsMadeForRoom($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $bookings
        ]);
    }

    public function getCancelledBookingsAction(Request $request)
    {
        $roomId = $request->query->get("roomId");
        $dateFrom = $request->query->get("dateFrom");
        $dateTo = $request->query->get("dateTo");

        $roomManager = $this->container->get("room_manager_api");

        $cancelledBookings = $roomManager->getCancelledBookings($roomId, $dateFrom, $dateTo);

        return new JsonResponse([
            'success' => true,
            'data' => $cancelledBookings
        ]);
    }
}
