<?php

namespace ApiBundle\Service;

use AppBundle\Entity\Bookings;
use AppBundle\Entity\Building;
use AppBundle\Entity\Room;
use AppBundle\Entity\Scans;
use AppBundle\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\Date;

class BookingManager
{
    const TIMES_24_HOURS = [
        0 => '08:00',
        1 => '09:00',
        2 => '10:00',
        3 => '11:00',
        4 => '12:00',
        5 => '13:00',
        6 => '14:00',
        7 => '15:00',
        8 => '16:00',
        9 => '17:00',
        10 => '18:00',
        11 => '19:00',
        12 => '20:00'
    ];

    const TIMES_12_HOURS = [
        0 => '08:00',
        1 => '09:00',
        2 => '10:00',
        3 => '11:00',
        4 => '12:00',
        5 => '01:00',
        6 => '02:00',
        7 => '03:00',
        8 => '04:00',
        9 => '05:00',
        10 => '06:00',
        11 => '07:00',
        12 => '08:00'
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns an array of time, that are currently free between 8am to 8pm.
     * (in 24hr format)
     *
     * @param $date
     * @param $roomId
     *
     * @return array
     */
    public function getAvailableTimesFrom($date, $roomId)
    {
        $em = $this->getEntityManager();

        try {
            $bookedTime = $em->createQuery(
                'SELECT b.time_from, b.time_to FROM AppBundle:Bookings b 
                      WHERE b.date = :date 
                      AND 
                      b.room_id = :room_id
                      AND
                      b.cancelled_time IS NULL'
            )
                ->setParameter('date', $date)
                ->setParameter('room_id', $roomId)->getResult();

            $times = $this->getTimes();

            if (!$bookedTime) {
                return $times;
            } else {
                foreach ($bookedTime as $booked) {

                    $timeFrom = date_format($booked['time_from'], "H:i");

                    $timeTo = date_format($booked['time_to'], "H:i");

                    foreach ($times as $key => $value) {
                        if ($times[$key] >= $timeFrom && $times[$key] < $timeTo) {
                            unset($times[$key]);
                        }
                    }
                }

                foreach ($times as $key => $value) {
                    $f = date('H:i', strtotime($value) + 60 * 60);

                    if (!in_array($f, $times)) {
                        unset($times[$key]);
                    }
                }
            }

        } catch (Exception $exception) {
            // log
        }

        return array_values($times);

    }

    /**
     *
     * Return an array showing, 1 to 4 hours free blocks,
     *
     * @param $times
     * @param $selectedTime
     *
     * @return array
     */
    public function getAvailableTimesTo($times, $selectedTime)
    {
        $intervalsArray = [1, 2, 3, 4];

        $possiableIntervals = [];

        try {

            foreach ($intervalsArray as $key => $value) {
                $currentTime = date('H:i', strtotime($selectedTime) + (60 * 60) * $key);

                if (in_array($currentTime, $times)) {
                    array_push($possiableIntervals, date('H:i', strtotime($currentTime) + 60 * 59));
                } else {
                    break;
                }
            }
        } catch (Exception $exception) {
            // Log
        }

        return $possiableIntervals;
    }

    /**
     * Returns all buildings name and id
     *
     * @return array
     */
    public function getBuilding()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT b.id, b.name FROM AppBundle:Building b');

        return $query->getArrayResult();
    }

    /**
     * Return a booking by ID
     *
     * @param $userId
     * @return array
     */
    public function getBookings($userId)
    {
        $em = $this->getEntityManager();

        $bookings = $em->createQuery(
            'SELECT b FROM AppBundle:Bookings b
                  WHERE b.user_id = :user_id '
        )->setParameter('user_id',$userId)->getResult();

        return $bookings;
    }

    /**
     * Add a scan, this will happen everytime a user tap an NFC card
     * with there device.
     *
     * @param $userId
     * @param $roomId
     * @return mixed
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addScans($userId, $roomId)
    {
        $em = $this->getEntityManager();
        $scan = new Scans();

        $userId = $this->getEntityManager()->getRepository(User::class)->find($userId);
        $roomId = $this->getEntityManager()->getRepository(Room::class)->find($roomId);

        $scan->setUserId($userId);
        $scan->setRoomId($roomId);
        $scan->setDateTime(new DateTime());

        $em->persist($scan);
        $em->flush();

        return $scan->getId();
    }

    /**
     * Createss a booking for a meeting rooom
     *
     * @param $userId
     * @param $roomId
     * @param $date
     * @param $timeFrom
     * @param $timeTo
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function makeBooking($userId, $roomId, $date, $timeFrom, $timeTo)
    {
        $em = $this->getEntityManager();

        $user = $this->getUser($userId);
        $room = $this->getRoom($roomId);

        $date = new DateTime($date);
        $timeFrom = new DateTime($timeFrom);
        $timeTo =  new DateTime($timeTo);

        $booking = new Bookings();
        $booking->setUserId($user);
        $booking->setRoomId($room);
        $booking->setDate($date);
        $booking->setTimeFrom($timeFrom);
        $booking->setTimeTo($timeTo);

        $bookingExists = $em->getRepository(Bookings::class)->findOneBy([
            'room_id' => $room,
            'date' => $date,
            'time_from' => $timeFrom,
            'time_to' => $timeTo
        ]);


        if (!$bookingExists) {
            $em->persist($booking);
            $em->flush();
            return true;
        } else {
            return false;
        }
    }


    private function getUser($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(User::class)->find($id);
    }

    private function getRoom($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Room::class)->find($id);
    }

    /**
     * Gets a building from ID
     *
     * @param $buildingId
     *
     * @return array
     */
    public function getRoomsFromBuildingId($buildingId)
    {
        $em = $this->getEntityManager();

        $building = $em->getRepository(Building::class)->find($buildingId);

        $query = $em->createQuery(
            'SELECT r FROM AppBundle:Room r WHERE r.building_id = :building')
        ->setParameter('building', $building->getId());

        return $query->getArrayResult();
    }

    /**
     * Returns a room, from NFC-CODE, this code will be saved on to
     * NFC tags.
     *
     * @param $nfcCode
     *
     * @return array
     */
    public function getRoomFromNfcCode($nfcCode)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT r FROM AppBundle:Room r WHERE r.nfc_code = :nfcCode')
            ->setParameter('nfcCode', $nfcCode);

        return $query->getArrayResult();

    }

    /**
     * Return all bookings assigned to a user
     *
     * @param $userId
     *
     * @return array
     */
    public function getBookingsForUser($userId)
    {
        $em = $this->getEntityManager();

        $user = $this->getUser($userId);

        $query = $em->createQuery(
            'SELECT b FROM AppBundle:Bookings b WHERE b.user_id = :user AND b.cancelled_time IS NULL'
        )->setParameter('user', $user);

        return $query->getResult();
    }

    /**
     * Returns booking for a user, todays bookings only.
     *
     * @param $userId
     *
     * @return array
     */
    public function getBookingForUserToday($userId)
    {
        $em = $this->getEntityManager();

        $user = $this->getUser($userId);

        $query = $em->createQuery(
            'SELECT b FROM AppBundle:Bookings b
                    WHERE b.user_id = :user 
                    AND 
                    b.date = :date')
            ->setParameter('user', $user)
            ->setParameter('date', date('Y/m/d'));

        return $query->getResult();
    }

    /**
     * Deletes a booking with booking_id
     *
     * @param $bookingId
     * @return mixed
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteBooking($bookingId)
    {
        // setting the timezone
        date_default_timezone_set('Europe/London');

        $em = $this->getEntityManager();

        $booking = $em->getRepository(Bookings::class)->find($bookingId);

        $booking->setCancelledTime(new DateTime());

        $em->persist($booking);
        $em->flush();

        return $booking->getId();
    }

    /**
     * @return array
     */
    private function getTimes()
    {
        return $this::TIMES_24_HOURS;
    }

    private function getEntityManager()
    {
        return $this->entityManager;
    }
}