<?php

namespace ApiBundle\Service;


use DateTime;
use Doctrine\ORM\EntityManager;

class RoomManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Return Entity Manager
     *
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Returns the top 5 most popular buildings
     *
     * @return array
     */
    public function topFivePopularRooms()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT DISTINCT r.name, COUNT(s.room_id) as scans from AppBundle:Scans s 
                  LEFT JOIN AppBundle:Room r 
                  where r.id = s.room_id 
                  GROUP BY r.id'
        )->setMaxResults(5);

        return $query->getResult();
    }

    /**
     * Returns the most popular Building
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function mostPopularBuilding()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT b.name, COUNT(s.room_id) as scans from AppBundle:Scans s 
                  LEFT JOIN AppBundle:Room r 
                  where r.id = s.room_id
                  LEFT JOIN AppBundle:Building b 
                  WHERE b.id = r.building_id
                  GROUP BY r.id
                  ORDER BY scans DESC'
        )->setMaxResults(1);

        return $query->getOneOrNullResult();
    }

    /**
     * Returns the Scans done on a certain room.
     *
     * @param $roomId
     * @param $timeFrom
     * @param $timeTo
     * @return mixed
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRoomScans($roomId, $timeFrom, $timeTo)
    {
        $em = $this->getEntityManager();

        $timeFrom = DateTime::createFromFormat('d/m/Y', $timeFrom);

        $timeTo = DateTime::createFromFormat('d/m/Y', $timeTo);

        $query = $em->createQuery(
            'SELECT COUNT(s.room_id) as scans from AppBundle:Scans s 
                  WHERE s.room_id = :roomId
                  AND s.date_time BETWEEN :timeFrom AND :timeTo')
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $timeFrom)
            ->setParameter('timeTo', $timeTo);

        return $query->getSingleResult();
    }

    /**
     * Breaks the the scans returned in to days, to show a line graph.
     * Returns in an array of days.
     *
     * @param $roomId
     * @param $timeFrom
     * @param $timeTo
     * @return array
     */
    public function getScansBreakDown($roomId, $timeFrom, $timeTo)
    {
        $em = $this->getEntityManager();


        $timeFrom = DateTime::createFromFormat('d/m/Y', $timeFrom);

        $timeTo = DateTime::createFromFormat('d/m/Y', $timeTo);

        $query = $em->createQuery(
            'SELECT DATE_FORMAT(s.date_time,\'%d/%m/%Y\') as date_time, COUNT(s.date_time) as scans from AppBundle:Scans s 
                  WHERE s.room_id = :roomId
                  AND s.date_time BETWEEN :timeFrom AND :timeTo
                  GROUP BY date_time')
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $timeFrom)
            ->setParameter('timeTo', $timeTo)->getResult();

        $results = [];

        foreach ($query as $key => $value) {

            $date = new DateTime($value['date_time']);

            array_push(
                $results,
                [
                    // *1000 needed for js timestamp
                    $date->getTimestamp() * 1000,
                    (int) $value['scans']
                ]
            );
        }

        return $results;
    }

    /**
     * Returns the scans done from buildings.
     * Returns scans done by day.
     *
     * @return array
     */
    public function getBuildingScans()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT b.name as building_name, s.date_time as date_time, COUNT(s.date_time) as scans from AppBundle:Scans s 
                  LEFT JOIN AppBundle:Room r 
                  where r.id = s.room_id
                  LEFT JOIN AppBundle:Building b 
                  WHERE b.id = r.building_id
                  GROUP BY b.name, date_time')->getResult();

        $results = [];

        foreach ($query as $key => $value) {

            $date = $value['date_time'];

            if (!array_key_exists($value['building_name'], $results)) {
                $results[$value['building_name']] = [];
            }

            array_push(
                $results[$value['building_name']],
                [
                    // *1000 needed for js timestamp
                    $date->getTimestamp() * 1000,
                    (int) $value['scans']
                ]
            );

        }

        $response = [];

        foreach ($results as $building => $data) {

            $response[] =  ['name' => $building, 'data' => $data];

        }

        return $response;
    }

    /**
     * Return the most popular date for room.
     *
     * @param $roomId
     * @param $dateFrom
     * @param $dateTo
     *
     * @return false|int|string
     */
    public function getMostPopularDateForRoom($roomId, $dateFrom, $dateTo)
    {
        $em = $this->getEntityManager();

        $dateFrom = DateTime::createFromFormat('d/m/Y', $dateFrom);

        $dateTo = DateTime::createFromFormat('d/m/Y', $dateTo);

        $results = $em->createQuery(
            "SELECT DATE_FORMAT(s.date_time,'%d/%m/%Y') as date, COUNT(s.room_id) as scans from AppBundle:Scans s 
                  WHERE s.room_id = :roomId
                  AND s.date_time BETWEEN :timeFrom AND :timeTo
                  GROUP by s.date_time")
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $dateFrom)
            ->setParameter('timeTo', $dateTo)
            ->getResult();

        if ($results == []) {
            return 'no data';
        } else {
            $response = [];

            foreach ($results as $key => $value) {

                $date = $value['date'];
                $scan = (int)$value['scans'];

                if (!array_key_exists($date, $response)) {
                    $response[$date] = $scan;
                } else {
                    $response[$date] = $response[$date] + $scan;
                }
            }

            $mostPopularDay = array_search(max($response), $response);

            return $mostPopularDay;
        }
    }

    /**
     * Returns the most popular time for rooms
     *
     * @param $roomId
     * @param $dateFrom
     * @param $dateTo
     *
     * @return string
     */
    public function getMostPopularTimeForRoom($roomId, $dateFrom, $dateTo)
    {
        $em = $this->getEntityManager();

        $dateFrom = DateTime::createFromFormat('d/m/Y', $dateFrom);

        $dateTo = DateTime::createFromFormat('d/m/Y', $dateTo);

        $results = $em->createQuery(
            "SELECT DATE_FORMAT(s.date_time,'%H') as date, COUNT(s.date_time) as scans from AppBundle:Scans s 
                  WHERE s.room_id = :roomId
                  AND s.date_time BETWEEN :timeFrom AND :timeTo
                  GROUP by date")
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $dateFrom)
            ->setParameter('timeTo', $dateTo)
            ->getResult();


        if ($results == []) {
            return 'no data';
        } else {
            $time = 0;
            $highestScan = 0;

            foreach ($results as $key => $value) {

                $date = $value['date'];
                $scan = (int)$value['scans'];

                if ($highestScan < $scan) {
                    $highestScan = $scan;
                    $time = $date;
                }
            }

            return $time.':00';
        }
    }

    /**
     * gets all the buildings totals scans
     *
     * @return array
     */
    public function getAllBuildingsTotalScans()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT b.name as building_name, COUNT(s.room_id) as scans from AppBundle:Scans s 
                  LEFT JOIN AppBundle:Room r 
                  where r.id = s.room_id
                  LEFT JOIN AppBundle:Building b 
                  WHERE b.id = r.building_id
                  GROUP BY b.name, s.date_time')->getResult();

        $results = [];

        foreach ($query as $key => $value) {

            $buildingName = $value['building_name'];
            $scans = (int) $value['scans'];

            if (!array_key_exists($value['building_name'], $results)) {
                $results[$buildingName] = $scans ;
            } else {
                $results[$buildingName] = $results[$buildingName] + $scans;
            }
        }

        $response = [];

        foreach ($results as $key => $value) {
            array_push($response, [ $key, $value ]);
        }


        return $response;
    }

    /**
     * Returns array of all the sucessful bookings made for a room.
     *
     * @param $roomId
     * @param $dateFrom
     * @param $dateTo
     *
     * @return array
     */
    public function getBookingsMadeForRoom($roomId, $dateFrom, $dateTo)
    {
        $em = $this->getEntityManager();

        $dateFrom = DateTime::createFromFormat('d/m/Y', $dateFrom);

        $dateTo = DateTime::createFromFormat('d/m/Y', $dateTo);

        $query = $em->createQuery(
            "SELECT DATE_FORMAT(b.date,'%d/%m/%Y') as date , COUNT(b) as bookings from AppBundle:Bookings b 
                  WHERE b.room_id = :roomId
                  AND b.cancelled_time IS NULL 
                  AND b.date BETWEEN :timeFrom AND :timeTo 
                  GROUP by b.date")
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $dateFrom)
            ->setParameter('timeTo', $dateTo)
            ->getResult();

        $result = [];

        foreach ($query as $key => $value) {

            array_push($result, [ $value['date'], (int) $value['bookings'] ]);
        }

        return $result;
    }

    /**
     * Returns all the cancelled bookings for a room.
     *
     * @param $roomId
     * @param $dateFrom
     * @param $dateTo
     *
     * @return array
     */
    public function getCancelledBookings($roomId, $dateFrom, $dateTo)
    {
        $em = $this->getEntityManager();

        $dateFrom = DateTime::createFromFormat('d/m/Y', $dateFrom);

        $dateTo = DateTime::createFromFormat('d/m/Y', $dateTo);

        $query = $em->createQuery(
            "SELECT DATE_FORMAT(b.date,'%d/%m/%Y') as date , COUNT(b) as bookings from AppBundle:Bookings b 
                  WHERE b.room_id = :roomId
                  AND b.cancelled_time IS NOT NULL 
                  AND b.date BETWEEN :timeFrom AND :timeTo
                  GROUP by b.date")
            ->setParameter(':roomId', $roomId)
            ->setParameter('timeFrom', $dateFrom)
            ->setParameter('timeTo', $dateTo)
            ->getResult();

        $result = [];

        foreach ($query as $key => $value) {

            array_push($result, [ $value['date'], (int) $value['bookings'] ]);
        }

        return $result;
    }
}