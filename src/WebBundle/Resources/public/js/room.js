$(document).ready(function () {

    // roomDropDown();
    roomDropDownLoader();

    $('#submit').click(function() {
        roomScans();
        scanBreakDown();
        mostPopularTime();
        mostPopularDate();
        bookingsMade();
        bookingCancelled();
    });

});

function roomDropDownLoader() {
    $('#buildingDropDown').change(function(event) {
        event.preventDefault();

        var buildingId = $('#buildingDropDown').val();
        var roomDropDownBox = $('#roomDropDown');
        var fromDatePicker = $('#fromDatePicker');
        var submit = $('#submit');

        var pathname = window.location.origin;

        var url = pathname + '/api/v1/room';

        roomDropDownBox.empty();

        if (buildingId !=  "") {
            jQuery.ajax({
                method: 'GET',
                url: url,
                data: {
                    "apikey": document.apiKey,
                    "buildingId": buildingId
                },
                dataType: 'json',
                success: function (data) {

                    var roomDropDownList = document.getElementById("roomDropDown");

                    $.each(data.data, function (index, value) {
                        var option = document.createElement("option");
                        option.text = value.name;
                        option.value = value.id;
                        option.innerText = value.name;

                        roomDropDownList.appendChild(option);
                    });

                    roomDropDownBox.removeAttr("disabled");
                    fromDatePicker.removeAttr("disabled");
                    submit.removeAttr("disabled");
                }
            })
        } else {
            roomDropDownBox.attr("disabled", "disabled");
            fromDatePicker.attr("disabled", "disabled");
            submit.attr("disabled", "disabled");
        }
    });
}

function scanBreakDown() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/break_down_scans';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function(data) {

            if (data.data.length > 0) {

                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: 'Break Down of Scans'
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { // don't display the dummy year
                            month: '%e. %b',
                            year: '%b'
                        },
                        title: {
                            text: 'Date'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Scans'
                        },
                        allowDecimals: false,
                        min: 0
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Scans',
                        data: data.data
                    }]
                });
            } else {
                Highcharts.chart('scansBreakDownContainer', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Break Down of Scans'
                    },
                    series: [{
                        data: []
                    }]
                });
            }
        }
    });

}

function mostPopularDate() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/popular_date';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();
    var container = $('#popularContainer');

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function(data) {

            var responseDate = data.data;

            var dateText = $('#mostPopularDate');

            dateText.text(responseDate);

            container.show();
        }
    });
}

function mostPopularTime() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/popular_time';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',
        success: function(data) {

            var responseDate = data.data;

            var dateText = $('#mostPopularTime');

            dateText.text(responseDate);
        }
    });
}

function bookingsMade() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/bookings_made';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',

        success: function (data) {
            var data = data.data;

            Highcharts.chart('bookingsMadeContainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Bookings successfully made'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of bookings'
                    },
                    allowDecimals: false
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: 'Bookings',
                    data: data,

                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        }

    });
}

function bookingCancelled() {

    var pathname = window.location.origin;

    var url = pathname + '/api/v1/room/cancelled_bookings';

    var roomId = $('#roomDropDown').val();
    var dateFrom = $('#fromDatePicker').val();
    var dateTo = $('#toDatePicker').val();

    jQuery.ajax({
        method: 'GET',
        url: url,
        data: {
            "apikey": document.apiKey,
            "roomId": roomId,
            "dateFrom": dateFrom,
            "dateTo": dateTo
        },
        dataType: 'json',

        success: function (data) {
            var data = data.data;

            Highcharts.chart('cancelledBookingsContainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Bookings Cancelled.'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of bookings'
                    },
                    allowDecimals: false
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: 'Cancelled Bookings',
                    data: data,

                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        }

    });
}