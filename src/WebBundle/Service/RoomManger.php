<?php

namespace WebBundle\Service;

use AppBundle\Entity\Building;
use AppBundle\Entity\Room;
use Doctrine\ORM\EntityManager;

class RoomManger
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAllRooms()
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Room::class)->findAll();
    }


    public function getRoomById($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Room::class)->find($id);
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}