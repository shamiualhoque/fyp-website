<?php

namespace WebBundle\Service;

use AppBundle\Entity\Building;
use Doctrine\ORM\EntityManager;

class BuildingManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BuildingManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAllBuildings()
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Building::class)->findAll();
    }

    public function getBuilding($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Building::class)->find($id);
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}