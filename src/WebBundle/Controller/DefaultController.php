<?php

namespace WebBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /** @var User $user */
        $currentUser = $this->getUser();

        $user = $this->getDoctrine()->getRepository(User::class)->find($currentUser);

        $apiKey = $user->getApiKey();

        return $this->render(
            'WebBundle:Default:index.html.twig',
            [
                'api_key' => $apiKey
            ]
        );
    }

    public function roomAction()
    {
        $buildingManager = $this->container->get('building_manager');

        $buildings = $buildingManager->getAllBuildings();

        /** @var User $user */
        $currentUser = $this->getUser();

        $user = $this->getDoctrine()->getRepository(User::class)->find($currentUser);

        $apiKey = $user->getApiKey();

        return $this->render(
            'WebBundle:Rooms:index.html.twig',
            [
                'bulidings' => $buildings,
                'api_key' => $apiKey
            ]
        );
    }

    public function checkUserAction(Request $request)
    {
        $username = $request->query->get('username');
        $password = $request->query->get('password');

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $username]);

        if (!$user) {
            return new JsonResponse([
                'success' => false
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($user) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            $bool = $encoder->isPasswordValid($user->getPassword(), $password, null);

            if ($bool == false) {
                return new JsonResponse([
                    'success' => false,
                ], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse([
                'success' => true,
                'data' => [
                    'id' => $user->getId(),
                    'username' => $username,
                    'apikey' => $user->getApiKey()
                ]
            ], Response::HTTP_OK);

        } else {
            return new JsonResponse([
                'success' => false
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function settingAction()
    {
        return $this->render('WebBundle:Default:setting.html.twig');
    }
}
