<?php

require __DIR__.'/vendor/autoload.php';

$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://127.0.0.1:8000/',
    'defaults' => [
        'exceptions' => false
    ]
]);

$response = $client->get('/api/programmers');

echo $response->getBody()->getContents();
echo "\n\n";